// leitura analogica de um sensor LDR, escrita no LCD e envio para o RPi via comunicacao serial
// Joao Aguirre jan/2014

#include <LiquidCrystal.h>

const int LDR = 5; // define o pino no qual esta ligado o sensor LDR
const float reference = 5.0; // definicao da tensao de referencia
const float resolution = 1024.; // resolucao do conversor AD de entrada do Arduino
const float factor = reference / resolution; // definicao do fator de conversao de leitura

int reading = 0; // variavel para receber a leitura da tensao
float tension = 0.0; // variavel para receber o valor calculado de tensao no LDR

LiquidCrystal lcd(12, 11, 10, 5, 4, 3, 2); // cria um objeto do tipo LiquidCristal e configura os pinos do 
                                           // Arduino para se comunicar com o LCD

void setup()
{
  // inicializa LCD
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("tensao LDR [V]");
  // inicializa serial
  Serial.begin(9600); // abre a porta serial para envio e dados a 9600 bps                        
  Serial.println("tensao LDR [V]");
}
    
void loop()
{
  // leitura do sinal e calculo da queda de tensao no LDR
  reading = analogRead(LDR); 
  tension = reference - factor * (float)(reading);
  // escrita do resultado no LCD
  lcd.setCursor(5, 1);
  lcd.print(tension);
  // escrita do resultdo na serial
  Serial.println(tension);
  
  delay(1000); // aguarda 1 segundo
}
