// monitor de temperatura e umidade relativa
// Joao Aguirre abr2014

#include <LiquidCrystal.h>
#include <Dht11.h>

const int DHT_DATA_PIN = 12;
float temperature = 0.0;
float humidity = 0.0;

Dht11 sensor(DHT_DATA_PIN);
LiquidCrystal lcd(10, 9, 8, 5, 4, 3, 2);

void setup()
{
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("teste LCD");  
} 

void loop()
{
  lcd.setCursor(0, 1);
  sensor.read();
  temperature = sensor.getTemperature();
  lcd.print(temperature);
  delay(2500);
}
