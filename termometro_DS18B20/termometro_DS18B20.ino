// tutorial para uso do termometro DS18B20
// modificado do tutorial para o sensor DS18S20 em http://playground.arduino.cc/Learning/OneWire
#include <OneWire.h>

const int sensorPin = 10;

byte present = 0;
byte data[12];
byte addr[8];


OneWire sensor(10);  // sensor no pino 10

void setup(void) 
{
  // inicializacao da porta serial
  Serial.begin(9600);
  // inicializacao e busca do sensor
  sensor.reset();
  // busca no 1-wire
  if (!sensor.search(addr)) 
  {
    Serial.print("nenhum sensor encontrado\n");
  }
  // verificacao do tipo do sensor
  if ( addr[0] == 0x28) 
  {
      Serial.print("sensor DS18B20 encontrado\n");
  }
  else
  {
     Serial.print("dispositivo nao reconhecido\n"); 
  }
  // verificacao do CRC do sensor
  if ( OneWire::crc8(addr, 7) != addr[7])
  {
      Serial.print("CRC invalido!\n");
      return;
  }

void loop(void) {
  byte i;



  Serial.print("R=");
  for( i = 0; i < 8; i++) {
    Serial.print(addr[i], HEX);
    Serial.print(" ");
  }

  if ( OneWire::crc8( addr, 7) != addr[7]) {
      Serial.print("CRC is not valid!\n");
      return;
  }

  if ( addr[0] == 0x10) {
      Serial.print("Device is a DS18S20 family device.\n");
  }
  else if ( addr[0] == 0x28) {
      Serial.print("Device is a DS18B20 family device.\n");
  }
  else {
      Serial.print("Device family is not recognized: 0x");
      Serial.println(addr[0],HEX);
      return;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1);         // start conversion, with parasite power on at the end

  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.

  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  Serial.print("P=");
  Serial.print(present,HEX);
  Serial.print(" ");
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
  Serial.print(" CRC=");
  Serial.print( OneWire::crc8( data, 8), HEX);
  Serial.println();
}
